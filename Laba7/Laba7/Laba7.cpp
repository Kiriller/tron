﻿#include <SFML/Graphics.hpp>

struct Point
{
	int x, y;
	Point(int x, int y) : x(x), y(y) {}
};

sf::CircleShape circle(int x, int y, int r)
{
	sf::CircleShape circle;
	circle.setRadius(r);
	circle.setOrigin(r, r);
	circle.setPosition(x, y);
	return circle;
}

int main(int, char const**)
{
	sf::RenderWindow window(sf::VideoMode(800, 600), "SFML window");
	window.setFramerateLimit(60);

	int x = 400, y = 300;
	int vx = 0, vy = 0;

	std::vector<Point> trail;

	while (window.isOpen())
	{
		//системные события
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();
		}

		bool up = sf::Keyboard::isKeyPressed(sf::Keyboard::Up);
		bool down = sf::Keyboard::isKeyPressed(sf::Keyboard::Down);
		bool left = sf::Keyboard::isKeyPressed(sf::Keyboard::Left);
		bool right = sf::Keyboard::isKeyPressed(sf::Keyboard::Right);

		if (up)
		{
			vx = 0;
			vy = -5;
		}
		if (down)
		{
			vx = 0;
			vy = 5;
		}
		if (left)
		{
			vy = 0;
			vx = -5;
		}
		if (right)
		{
			vy = 0;
			vx = 5;
		}
		if (up && left)
		{
			vx = -5;
			vy = -5;
		}
		if (up && right)
		{
			vx = 5;
			vy = -5;
		}
		if (down && left)
		{
			vx = -5;
			vy = 5;
		}
		if (down && right)
		{
			vx = 5;
			vy = 5;
		}

		x += vx;
		y += vy;

		trail.push_back(Point(x, y));

		window.clear();

		window.draw(circle(x, y, 10));

		for (int i = 1; i < trail.size(); i++)
		{
			Point p = trail[i - 1];
			Point n = trail[i];
			sf::Vertex line[] =
			{
			sf::Vertex(sf::Vector2f(p.x, p.y)),
			sf::Vertex(sf::Vector2f(n.x, n.y))
			};
			window.draw(line, 2, sf::Lines);
		}


		window.display();
	}


	return 0;
}
